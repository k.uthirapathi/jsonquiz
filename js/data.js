const jsonData = [
    {
        "q":"1. JSON stands for ________________",
        "opt1":"Java Standard Output Network",
        "opt2":"JavaScript Object Notation",
        "opt3":"JavaScript Output Name",
        "opt4":"Java Source Open Network",
        "answer":"JavaScript Object Notation"
    },
    {
        "q":"2. Which is correct format of writting JSON name/value pair",
        "opt1":"'name : value'",
        "opt2":"name = 'value'",
        "opt3":"name = \"value\"",
        "opt4":"\"name\" : \"value\"",
        "answer":"\"name\" : \"value\""
    },
    {
        "q":"3. Which of the following is not a type in JSON?",
        "opt1":"date",
        "opt2":"Object",
        "opt3":"Array",
        "opt4":"string",
        "answer":"date"
    },
    {
        "q":"4. Who is the Father of JSON ?",
        "opt1":"Douglas Crockford",
        "opt2":"Rasmus Lerdorf",
        "opt3":"Douglas Michel",
        "opt4":"Dennis Ritchie",
        "answer":"Douglas Crockford"
    },
    {
        "q":"5. What is correct MIME type for JSON ?",
        "opt1":"application/js",
        "opt2":"application/json",
        "opt3":"json/mime",
        "opt4":"application/xml",
        "answer":"application/json"
    },
    {
        "q":"6. What extension in used to save a JSON file?",
        "opt1":".json",
        "opt2":".js",
        "opt3":".javaN",
        "opt4":".on",
        "answer":".js"
    },
    {
        "q":"7. For What is a JSONStringer used for?",
        "opt1":"To quickly create number to JSON text.",
        "opt2":"To create JSON ordered pairs.",
        "opt3":"To converts JSON to Java strings",
        "opt4":"To quickly create JSON text",
        "answer":"To quickly create JSON text"
    },
   
];