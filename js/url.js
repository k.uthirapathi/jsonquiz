

//////////////////////////////////////////
const select = document.querySelector(".myselect");
const options = document.querySelectorAll(".myselect option");
if (select) {
   
    select.addEventListener("change", function() {
      const url = this.options[this.selectedIndex].value;
      if (url) {
        localStorage.setItem("url", url);
        location.href = url;
      }
    });
   
    for (const option of options) {
      const url = option.value;
     
      if (localStorage.getItem("url") === url) {
  
        option.setAttribute("selected", url)
        
        break;
      }
    }

    
   
    
  }
  let preference = localStorage.getItem('url');

  window.addEventListener("load", () => {
   
    if (null!== preference) {
       
      localStorage.setItem("url", "Default Value");
    }
  });

