// Advance Js

// Nested function's scope
// Closures
// Currying
// this keyword
// Prototype
// Prototype Inheritance
// Class
// Iterables and Iterators
// Generators

//////Scope///////

//Block scope--> Variable declared  inside a pair of curly braces 
//               canot be accessed from outside the block

//Function scope--> Variables declared inside a function 
                    //cannot be accessed from outside the function 


//Global scope--> Globally scoped variables can be accessed 
                //inside a block or function 
let a =10;
function outer(){
    let b= 10;
    function inner(){
        let c= 30;
        console.log(a,b,c);
    }
    inner();
}

outer();

// Cluster
function outer1(){
     let counter = 0;
     function inner1(){
         counter += 1;

         console.log(counter);
     }
   return inner1;
     

}

const fn = outer1();
fn();
fn();

//spread oparator

const example1 = [1,2,3,4,5,6,7];
const example2 = [...example1];
example2.push(10);
//console.log(example2);


// get last word of string
const targets = document.querySelector('target');
const getLastIndex = getLastWord(targets.innerText.value);

function getLastWord(txt){
 const words = txt.split('');
 console.log(words);
}
